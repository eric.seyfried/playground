FROM nginx:alpine
# Set working directory
WORKDIR /var/www/public

# Copy existing application directory contents
COPY ./public /var/www/public

COPY ./nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

COPY ./deploy.sh ./
RUN chmod 777 ./deploy.sh
# Copy existing application directory permissions
# COPY --chown=www:www . /var/www
# # Change current user to www
# USER www