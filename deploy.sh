sshpass  -p $1 ssh -o StrictHostKeyChecking=no deployer@134.122.120.47 <<-'ENDSSH'   
   docker login -u gitlab-ci-token -p $2 registry.gitlab.com
   docker stop webapp-ws
   docker rm webapp-ws
   docker pull registry.gitlab.com/eric.seyfried/playground
   docker run --name webapp-ws -p 80:80 -d registry.gitlab.com/eric.seyfried/playground
ENDSSH